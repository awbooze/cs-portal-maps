# cs-portal-maps
Building maps for the CS Portal.

## First-time Build
- Clone the repository
- Make sure Django is installed. Tested and confirmed working with Django 3.2.7.
- Change directory to `home/campusmaps`
- Run `python manage.py migrate` to migrate database state from python files.
- Run `python manage.py runserver` to run the development server.
